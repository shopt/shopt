/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

/**
 * Data object for an entry on the shopping list.  They are used in TreeSet
 * and thus have to implement Comparable.
 */
public class ListEntry implements Comparable<ListEntry>
{

  /**
   * The internal database ID.  This is used for delete or similar
   * operations that require cross-references.  It may be -1 if we
   * are inserting this entry.
   */
  private final long id;

  /** The item to be bought.  */
  private final Item item;

  /** The shop at which it should be bought.  */
  private final Shop shop;

  /** The extra tag value (if any).  */
  private final String tag;

  /** The item's price at the given shop.  */
  private final Double price;

  /**
   * Construct a new entry object.
   * @param key The database ID.
   * @param itm The item object.
   * @param s The shop object.
   * @param t The tag string.
   * @param p The item price.
   */
  public ListEntry (long key, Item itm, Shop s, String t, Double p)
  {
    id = key;
    item = itm;
    shop = s;
    tag = t;
    price = p;
  }

  /**
   * Construct a new entry object with -1 ID.  This is used when
   * inserting a new entry into the DB.
   * @param itm The item object.
   * @param s The shop object.
   * @param t The tag string.
   */
  public ListEntry (Item itm, Shop s, String t)
  {
    this (-1, itm, s, t, null);
  }

  /**
   * Get the ID.  This fails if no ID is set.
   * @return The entry's ID.
   */
  public long getID ()
  {
    assert (id != -1);
    return id;
  }

  /**
   * Get the item.
   * @return The item to be bought.
   */
  public Item getItem ()
  {
    return item;
  }

  /**
   * Get the shop.
   * @return The shop where we want to buy.
   */
  public Shop getShop ()
  {
    return shop;
  }

  /**
   * Get the tag value.
   * @return The tag value, may be null.
   */
  public String getTag ()
  {
    return tag;
  }

  /**
   * Return the price.
   * @return The price (may be null if unknown).
   */
  public Double getPrice ()
  {
    return price;
  }

  /**
   * Compare for equality.  This uses the row IDs and should only
   * ever be called for IDs other than -1 (i. e., entries already in the DB).
   * @param o The other object.
   * @return True iff both have the same ID.
   */
  @Override
  public boolean equals (Object o)
  {
    if (!(o instanceof ListEntry))
      return false;
    final ListEntry e = (ListEntry) o;

    assert (id != -1 && e.id != -1);
    return id == e.id;
  }

  /**
   * Compare to another object.  This compares by ID, so it should not be
   * used for IDs of -1.  This is good to get "some" ordering for use
   * in a TreeMap, which is all we want.
   * @param o The other object.
   * @return Comparation result.
   */
  @Override
  public int compareTo (ListEntry o)
  {
    assert (id != -1 && o.id != -1);
    if (id < o.id)
      return -1;
    if (id > o.id)
      return 1;
    return 0;
  }

}
