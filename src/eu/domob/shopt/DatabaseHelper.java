/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.util.Log;

/**
 * Database helper class.  This defines the database format.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{

  /** Database name.  */
  private static final String DB_NAME = "shopt.db";
  /** Current database version.  */
  private static final int DB_VERSION = 1;

  /**
   * Construct it in the given context.
   * @param c Context.
   */
  public DatabaseHelper (Context c)
  {
    super (c, DB_NAME, null, DB_VERSION);
  }

  /**
   * Create the database.
   * @param db Database.
   */
  @Override
  public void onCreate (SQLiteDatabase db)
  {
    Log.i (Shopt.TAG, "Creating database.");

    /* The table holding all known shops.  */
    db.execSQL ("CREATE TABLE `shops`"
                + " (`_id`  INTEGER PRIMARY KEY,"
                + "  `name` TEXT NOT NULL)");

    /* Table with all known items, without further information
       about associations with particular shops.  */
    db.execSQL ("CREATE TABLE `items`"
                + " (`_id`  INTEGER PRIMARY KEY,"
                + "  `name` TEXT NOT NULL)");

    /* Associate items with shops:  Where can each item be bought
       and what price does it have there.  */
    /* FIXME: Can't we use WITHOUT ROWID here?  */
    db.execSQL ("CREATE TABLE `prices`"
                + " (`item`   INTEGER NOT NULL,"
                + "  `shop`   INTEGER NOT NULL,"
                + "  `price`  REAL NULL,"
                + "  PRIMARY KEY (`item`, `shop`))");

    /* The shopping list itself.  Item and/or shop are not a primary
       key and we don't impose uniqueness.  It is fine (due to simplicity
       and flexibility) to have multiple entries for the same item and shop.
       This can possibly be useful with different 'tag' values.  */
    db.execSQL ("CREATE TABLE `list`"
                + " (`_id`  INTEGER PRIMARY KEY,"
                + "  `shop` INTEGER NOT NULL,"
                + "  `item` INTEGER NOT NULL,"
                + "  `tag`  TEXT NULL)");

    /* Construct a view that joins together some tables in order to
       get everything we need about the shopping list entries (in
       particular, item/shop names and price).  */
    db.execSQL ("CREATE VIEW `extended_list` AS"
                + " SELECT l.*, s.`name` AS `shop_name`,"
                + "        i.`name` AS `item_name`, p.`price`"
                + "   FROM `list` l"
                + "     INNER JOIN `shops` s ON s.`_id` = l.`shop`"
                + "     INNER JOIN `items` i ON i.`_id` = l.`item`"
                + "     INNER JOIN `prices` p"
                + "       ON p.`item` = l.`item` AND p.`shop` = l.`shop`");
  }

  /**
   * Update the database.
   * @param db Database.
   * @param oldVersion The old version.
   * @param newVersion The new version.
   */
  @Override
  public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion)
  {
    /* This should not be called since we haven't changed the db format yet.  */
    assert (false);
  }

}
