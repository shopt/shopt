/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.content.Intent;

import android.os.Bundle;

import android.widget.ListView;
import android.widget.TextView;

/**
 * Activity to show details for an item.  This is the main UI to
 * select shops at which an item is offerend and enter / view the prices.
 */
public class ItemDetails extends BaseActivity
{

  /** The item for which the details are shown.  */
  private Item itm;

  /** Data adapter for the shown list.  */
  private ItemDetailsAdapter data;

  /**
   * Create the activity.
   * @param savedInstanceState The saved state.
   */
  @Override
  public void onCreate (Bundle savedInstanceState)
  {
    super.onCreate (savedInstanceState);
    setContentView (R.layout.item_details);

    /* Interpret the intent.  */
    final Intent intent = getIntent ();
    itm = (Item) intent.getSerializableExtra (Shopt.MESSAGE_ITEM);

    /* Show item name as "heading".  */
    final TextView tv = (TextView) findViewById (R.id.itemName);
    tv.setText (itm.getName ());

    /* Set up the list of shops/prices.  */
    final ListView list = (ListView) findViewById (R.id.itemDetailsList);
    data = new ItemDetailsAdapter (this);
    list.setAdapter (data);
  }

  /**
   * Resume operation after being paused.
   */
  @Override
  protected void onResume ()
  {
    super.onResume ();

    data.setData (db.getShops (), db.getItemShops (itm));
    data.notifyDataSetChanged ();
  }

  /**
   * When paused, save current data.
   */
  @Override
  protected void onPause ()
  {
    db.setItemShops (itm, data.getData ());
    super.onPause ();
  }

}
