/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import java.io.Serializable;

/**
 * Data object for an item.  This class is serialisable so that it can
 * be passed via an intent to the "item details" activity.  We could
 * pass the ID instead, but passing the object seems "more natural".
 */
public class Item implements Serializable
{

  /** Serialising version.  */
  private static final long serialVersionUID = 0l;

  /**
   * The internal database ID.  This is used for delete or similar
   * operations that require cross-references.  It may be -1 if we
   * are inserting this shop.
   */
  private final long id;

  /** The item's name.  */
  private final String name;

  /**
   * Construct a new Item object.
   * @param key The database ID.
   * @param nm The name string.
   */
  public Item (long key, String nm)
  {
    id = key;
    name = nm;
  }

  /**
   * Construct a new Item object with -1 ID.  This is used when
   * inserting a new item into the DB.
   * @param nm The name string.
   */
  public Item (String nm)
  {
    this (-1, nm);
  }

  /**
   * Get the name.
   * @return The item's name.
   */
  public String getName ()
  {
    return name;
  }

  /**
   * Get the ID.  This fails if no ID is set.
   * @return The item's ID.
   */
  public long getID ()
  {
    assert (id != -1);
    return id;
  }

  /**
   * Convert to string.  This simply returns the item's name.  It is used
   * by ArrayAdapter that we will use.
   * @return String representation of the item.
   */
  @Override
  public String toString ()
  {
    return getName ();
  }

}
