/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.os.Bundle;

import android.util.SparseBooleanArray;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

/**
 * Activity for editing the list of items.
 */
public class ListOfItems extends BaseActivity
  implements AdapterView.OnItemLongClickListener
{

  /** The list view.  */
  private ListView view;

  /** Array adapter with the item list data.  */
  private ArrayAdapter<Item> data;

  /** Input field for the new item's name.  */
  private EditText itemNameInput;

  /** As-you-type filter object.  */
  private FilterUpdater filter;

  /**
   * Create the activity.
   * @param savedInstanceState The saved state.
   */
  @Override
  public void onCreate (Bundle savedInstanceState)
  {
    super.onCreate (savedInstanceState);
    setContentView (R.layout.items);

    /* Set up the list view with empty data (for now).  */
    view = (ListView) findViewById (R.id.itemList);
    view.setOnItemLongClickListener (this);
    data = new ArrayAdapter<Item> (this,
                  android.R.layout.simple_list_item_multiple_choice);
    view.setAdapter (data);

    itemNameInput = (EditText) findViewById (R.id.itemName);
    filter = new FilterUpdater (itemNameInput, data);
  }

  /**
   * Clean up on destroy.
   */
  @Override
  public void onDestroy ()
  {
    filter.disconnect ();
    super.onDestroy ();
  }

  /**
   * Resume operation after being paused.
   */
  @Override
  protected void onResume ()
  {
    super.onResume ();
    updateList ();
  }

  /**
   * Update the data shown in the list from the DB.
   */
  private void updateList ()
  {
    final Item[] items = db.getItems ();
    data.clear ();
    for (Item itm : items)
      data.add (itm);
    data.notifyDataSetChanged ();
    filter.update ();
  }

  /**
   * Handle "add" button click.
   * @param v The button view.
   */
  public void onAddClick (View v)
  {
    final String name = itemNameInput.getText ().toString ();
    if (!name.isEmpty ())
      {
        final Item itm = db.insertItem (new Item (name));
        itemNameInput.setText ("");
        updateList ();

        if (itm != null)
          showItemDetails (itm);
      }
  }

  /**
   * Create the options menu.
   * @param menu Inflate menu there.
   * @return True since we always succeed.
   */
  @Override
  public boolean onCreateOptionsMenu (Menu menu)
  {
    final MenuInflater inflater = getMenuInflater ();
    inflater.inflate (R.menu.items, menu);
    return true;
  }

  /**
   * Perform action on click in menu.
   * @param itm Selected item.
   * @return True iff event was processed.
   */
  @Override
  public boolean onOptionsItemSelected (MenuItem itm)
  {
    switch (itm.getItemId ())
      {
      case R.id.itemListDelete:
        final SparseBooleanArray selected = view.getCheckedItemPositions ();
        for (int i = 0; i < data.getCount (); ++i)
          if (selected.get (i))
            db.deleteItem (data.getItem (i));
        view.clearChoices ();
        updateList ();
        return true;

      default:
        return super.onOptionsItemSelected (itm);
      }
  }

  /**
   * Handle item click.
   * @param parent Adapter view clicked on.
   * @param v The view clicked on.
   * @param pos The entry's position in the list.
   * @param id The entry's row id.
   * @return Always true.
   */
  @Override
  public boolean onItemLongClick (AdapterView<?> parent, View v,
                                  int pos, long id)
  {
    showItemDetails (data.getItem (pos));
    return true;
  }

}
