/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.text.Editable;
import android.text.TextWatcher;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

/**
 * Utility class that is used for as-you-type filtering.  It updates
 * the filter whenever the text view is changed.
 */
public class FilterUpdater implements TextWatcher
{

  /** The filter that is to be updated.  */
  private Filter filter;

  /** The text view that is watched.  */
  private TextView edit;

  /**
   * Construct the object.  This registers the event listener.
   * @param v The text view to watch.
   * @param f The Filterable object to update.
   */
  public FilterUpdater (TextView v, Filterable f)
  {
    filter = f.getFilter ();
    edit = v;

    edit.addTextChangedListener (this);
  }

  /**
   * Disconnect the listener.
   */
  public void disconnect ()
  {
    edit.removeTextChangedListener (this);
  }

  /**
   * Update the filtering.
   */
  public void update ()
  {
    filter.filter (edit.getText ());
  }

  /**
   * Text changed.  Update the filter.
   * @param s New test.
   * @param start Starting index of the change.
   * @param before Old length.
   * @param count New length.
   */
  @Override
  public void onTextChanged (CharSequence s, int start, int before, int count)
  {
    update ();
  }

  // Unused events.

  @Override
  public void beforeTextChanged (CharSequence s, int start, int before, int c)
  {}

  @Override
  public void afterTextChanged (Editable s)
  {}

}
