/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Custom adapter class for rows in the "add entry" list.
 */
public class AddEntryAdapter extends BaseAdapter
{

  /** Context for created views.  */
  private final Context context;

  /** Number format used for prices.  */
  private final NumberFormat priceFmt;

  /** List of shops (only those where the item can be bought).  */
  private List<Shop> shops;

  /** Mapping of shops to prices.  */
  private Map<Shop, Double> prices;

  /** The set of selected shops.  */
  private Set<Shop> selected;

  /**
   * Construct the adapter without data for now.
   * @param c The context to use.
   */
  public AddEntryAdapter (Context c)
  {
    context = c;
    shops = null;

    priceFmt = NumberFormat.getCurrencyInstance ();
  }

  /**
   * Set the data.  The list of shops will be filtered immediately
   * before setting the member variable, so that only shops also
   * present in the price map are stored.
   * @param ss List of all shops in the DB.
   * @param p Price map.
   * @param sel Initial selected set (may be null).
   */
  public void setData (Shop[] ss, Map<Shop, Double> p, Set<Shop> sel)
  {
    prices = p;

    Shop bestPriceAt = null;
    double bestPrice = 0.0;

    shops = new ArrayList<Shop> ();
    for (Shop s : ss)
      if (prices.containsKey (s))
        {
          shops.add (s);

          final Double curPrice = prices.get (s);
          if (curPrice != null
              && (bestPriceAt == null || bestPrice > curPrice))
            {
              bestPriceAt = s;
              bestPrice = curPrice;
            }
        }

    /* If we have already a selected set, take that and just remove entries
       not valid.  */
    selected = new TreeSet<Shop> ();
    if (sel != null)
      {
        for (Shop s : shops)
          if (sel.contains (s))
            selected.add (s);
        return;
      }

    /* Initialise selected set.  If we have only one shop, use it.  Otherwise,
       use the shop with best price (if any).  */
    if (shops.size () == 1)
      selected.add (shops.get (0));
    else if (bestPriceAt != null)
      selected.add (bestPriceAt);
  }

  /**
   * Get the set of selected shops.
   * @return Selected shop set.
   */
  public Set<Shop> getSelected ()
  {
    return selected;
  }

  /**
   * Return item count, which is the number of shops (in the list).
   * @return Number of shops in the dataset.
   */
  @Override
  public int getCount ()
  {
    if (shops == null)
      return 0;
    return shops.size ();
  }

  /**
   * Return the entry (shop) at the given position.
   * @param ind The index queried for.
   * @return The shop at this position.
   */
  @Override
  public Shop getItem (int ind)
  {
    return shops.get (ind);
  }

  /**
   * Return an entry (shop) ID.
   * @param ind The index queried for.
   * @return The entry's ID.
   */
  @Override
  public long getItemId (int ind)
  {
    return shops.get (ind).getID ();
  }

  /**
   * Construct a view for an entry.
   * @param ind The entry's index.
   * @param v A view that can be recycled.
   * @param parent The parent view group.
   */
  @Override
  public View getView (int ind, View v, ViewGroup parent)
  {
    if (v == null)
      {
        final LayoutInflater inf = LayoutInflater.from (context);
        v = inf.inflate (R.layout.add_entry_row, null);
        v.setTag (null);
      }

    /* We store the last row associated to a recycled view in the tag.
       If there is one, extract it and make sure to destroy
       the link between row and view.  */
    Row r;
    if (v.getTag () != null)
      {
        r = (Row) v.getTag ();
        r.unsetView ();
      }

    r = new Row (shops.get (ind), v);
    v.setTag (r);

    return v;
  }

  /* ************************************************************************ */
  /* Row.  */

  /**
   * Class for one of the rows (corresponding to a shop).  It holds the checkbox
   * to determine selectedness and updates the selected set accordingly.
   */
  private class Row implements View.OnClickListener
  {

    /** The shop it corresponds to.  */
    private final Shop shop;

    /** The checkbox view.  */
    private CheckBox check;

    /**
     * Construct it for the given view.
     * @param s The shop for this row.
     * @param v The full row view.
     */
    public Row (Shop s, View v)
    {
      shop = s;

      check = (CheckBox) v.findViewById (R.id.addFromShop);
      check.setText (shop.getName ());
      check.setOnClickListener (this);

      assert (prices.containsKey (shop));
      final Double p = prices.get (shop);

      final TextView priceView = (TextView) v.findViewById (R.id.itemPrice);
      if (p == null)
        priceView.setText ("?");
      else
        priceView.setText (priceFmt.format (p));
      
      check.setChecked (selected.contains (shop));
    }

    /**
     * Destroy link to view.  This is done when the view
     * is recycled.
     */
    public void unsetView ()
    {
      if (check != null)
        check.setOnClickListener (null);
      check = null;
    }

    /**
     * Handle click / toggle of the checkbox.
     * @param v Clicked view.
     */
    @Override
    public void onClick (View v)
    {
      assert (v == check);
      if (check.isChecked ())
        selected.add (shop);
      else
        selected.remove (shop);
    }

  }

}
