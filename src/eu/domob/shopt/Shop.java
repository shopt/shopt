/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

/**
 * Data object for a shop.  These are used as keys in a TreeMap
 * (since the tree map contains the shop-to-price mapping for items)
 * and thus have to implement Comparable.
 */
public class Shop implements Comparable<Shop>
{

  /**
   * The internal database ID.  This is used for delete or similar
   * operations that require cross-references.  It may be -1 if we
   * are inserting this shop.
   */
  private final long id;

  /** The shop's name.  */
  private final String name;

  /**
   * Construct a new Shop object.
   * @param key The database ID.
   * @param nm The name string.
   */
  public Shop (long key, String nm)
  {
    id = key;
    name = nm;
  }

  /**
   * Construct a new Shop object with -1 ID.  This is used when
   * inserting a new shop into the DB.
   * @param nm The name string.
   */
  public Shop (String nm)
  {
    this (-1, nm);
  }

  /**
   * Construct a Shop object without name.  These are used to represent
   * shops (with a known ID) internally at some places.  E. g., as keys
   * into a map.
   * @param key The shop's database ID.
   */
  public Shop (long key)
  {
    this (key, null);
  }

  /**
   * Get the name.
   * @return The shop's name.
   */
  public String getName ()
  {
    assert (name != null);
    return name;
  }

  /**
   * Get the ID.  This fails if no ID is set.
   * @return The shop's ID.
   */
  public long getID ()
  {
    assert (id != -1);
    return id;
  }

  /**
   * Convert to string.  This simply returns the shop's name.  It is used
   * by ArrayAdapter that we will use.
   * @return String representation of the shop.
   */
  @Override
  public String toString ()
  {
    return getName ();
  }

  /**
   * Compare for equality.  This uses the row IDs and should only
   * ever be called for IDs other than -1 (i. e., shops already in the DB).
   * @param o The other object.
   * @return True iff both have the same ID.
   */
  @Override
  public boolean equals (Object o)
  {
    if (!(o instanceof Shop))
      return false;
    final Shop s = (Shop) o;

    assert (id != -1 && s.id != -1);
    return id == s.id;
  }

  /**
   * Compare to another object.  This compares by ID, so it should not be
   * used for IDs of -1.  This is good to get "some" ordering for use
   * in a TreeMap, which is all we want.
   * @param o The other object.
   * @return Comparation result.
   */
  @Override
  public int compareTo (Shop o)
  {
    assert (id != -1 && o.id != -1);
    if (id < o.id)
      return -1;
    if (id > o.id)
      return 1;
    return 0;
  }

}
