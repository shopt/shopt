/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.app.Dialog;

import android.content.Intent;

import android.os.Bundle;

import android.text.method.LinkMovementMethod;

import android.util.Log;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.Set;

/**
 * Main activity for Shopt:  Show the shopping list itself.
 */
public class ShoppingList extends BaseActivity
{

  /** Dialog ID for about.  */
  private static final int DIALOG_ABOUT = 0;

  /** The entry box for adding new entries.  */
  private AutoCompleteTextView addEntry;

  /** Adapter for spinner entries.  */
  private ArrayAdapter<Item> itemData;

  /** The data adapter used.  */
  private ShoppingListAdapter data;

  /** Selected set, to store it between pause/resume calls.  */
  private Set<ListEntry> selected;

  /**
   * Create the activity.
   * @param savedInstanceState The saved state.
   */
  @Override
  public void onCreate (Bundle savedInstanceState)
  {
    super.onCreate (savedInstanceState);
    setContentView (R.layout.list);

    addEntry = (AutoCompleteTextView) findViewById (R.id.addListEntry);
    itemData = new ArrayAdapter<Item> (this,
                      android.R.layout.simple_dropdown_item_1line);
    addEntry.setAdapter (itemData);

    ExpandableListView list;
    list = (ExpandableListView) findViewById (R.id.shoppingList);
    data = new ShoppingListAdapter (this);
    list.setAdapter (data);
    selected = null;
  }

  /**
   * Resume operation after being paused.
   */
  @Override
  protected void onResume ()
  {
    super.onResume ();
    updateList ();
  }

  /**
   * Pause and store data before that.
   */
  @Override
  protected void onPause ()
  {
    selected = data.getSelected ();
    super.onPause ();
  }

  /**
   * Handle click on the "add entry" button.
   * @param v The button view.
   */
  public void onAddClick (View v)
  {
    final String name = addEntry.getText ().toString ();
    final Item itm = db.getItemByName (name);
    if (itm == null)
      {
        Log.i (Shopt.TAG,
               String.format ("Did not find appropriate item to add for '%s'.",
                              name));
        return;
      }

    addEntry.setText ("");
    final Intent intent = new Intent (this, AddEntry.class);   
    intent.putExtra (Shopt.MESSAGE_ITEM, itm);
    startActivity (intent);
  }

  /**
   * Create the options menu.
   * @param menu Inflate menu there.
   * @return True since we always succeed.
   */
  @Override
  public boolean onCreateOptionsMenu (Menu menu)
  {
    final MenuInflater inflater = getMenuInflater ();
    inflater.inflate (R.menu.list, menu);
    return true;
  }

  /**
   * Perform action on click in menu.
   * @param itm Selected item.
   * @return True iff event was processed.
   */
  @Override
  public boolean onOptionsItemSelected (MenuItem itm)
  {
    switch (itm.getItemId ())
      {
      case R.id.entryListDelete:
        selected = data.getSelected ();
        for (ListEntry e : selected)
          db.deleteListEntry (e);
        updateList ();
        return true;

      case R.id.showItems:
        startActivity (new Intent (this, ListOfItems.class));
        return true;

      case R.id.showShops:
        startActivity (new Intent (this, ListOfShops.class));
        return true;

      case R.id.showAbout:
        showDialog (DIALOG_ABOUT);
        return true;

      default:
        return super.onOptionsItemSelected (itm);
      }
  }

  /**
   * Create a dialog.
   * @param id Dialog to create.
   * @return The created dialog.
   */
  @Override
  public Dialog onCreateDialog (int id)
  {
    final Dialog dlg = new Dialog (this);
    TextView tv;

    switch (id)
      {
      case DIALOG_ABOUT:
        dlg.setContentView (R.layout.about);

        tv = (TextView) dlg.findViewById (R.id.aboutVersion);
        final String aboutVersion = getString (R.string.about_version);
        final String appName = getString (R.string.app_name);
        final String appVersion = getString (R.string.app_version);
        tv.setText (String.format (aboutVersion, appName, appVersion));

        tv = (TextView) dlg.findViewById (R.id.aboutLink1);
        tv.setMovementMethod (LinkMovementMethod.getInstance ());
        tv = (TextView) dlg.findViewById (R.id.aboutLink2);
        tv.setMovementMethod (LinkMovementMethod.getInstance ());

        dlg.setTitle (R.string.about_title);
        break;

      default:
        assert (false);
      }

    return dlg;
  }

  /**
   * Update the displayed list.
   */
  private void updateList ()
  {
    final Item[] items = db.getItems ();
    itemData.clear ();
    for (Item itm : items)
      itemData.add (itm);
    itemData.notifyDataSetChanged ();

    data.setData (db.getShops (), db.getListEntries (), selected);
    data.notifyDataSetChanged ();
  }

}
