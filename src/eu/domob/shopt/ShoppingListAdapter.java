/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Custom adapter for the shopping list.  It yields items and groups
 * them by shops at which we want to buy.
 */
public class ShoppingListAdapter extends BaseExpandableListAdapter
{

  /** BaseActivity used.  */
  private final BaseActivity context;

  /** Number format used for prices.  */
  private final NumberFormat priceFmt;

  /** List of shops (for grouping).  */
  private Shop[] shops;

  /** Map shops to their entries.  */
  private Map<Shop, List<ListEntry>> entries;

  /** The set of selected entries.  */
  private Set<ListEntry> selected;

  /**
   * Construct the adapter without data for now.
   * @param c The context (BaseActivity!) to use.
   */
  public ShoppingListAdapter (BaseActivity c)
  {
    context = c;
    shops = null;
    entries = null;

    priceFmt = NumberFormat.getCurrencyInstance ();
  }

  /**
   * Set the data.
   * @param ss List of all shops in the DB.
   * @param es List of all shopping list entries.
   * @param sel Initial selected set (may be null).
   */
  public void setData (Shop[] ss, ListEntry[] es, Set<ListEntry> sel)
  {
    shops = ss;

    entries = new TreeMap<Shop, List<ListEntry>> ();
    for (Shop s : shops)
      entries.put (s, new ArrayList<ListEntry> ());
    for (ListEntry e : es)
      {
        final Shop key = e.getShop ();
        assert (entries.containsKey (key));
        entries.get (key).add (e);
      }

    /* If we have already a selected set, take that and just remove entries
       not valid.  */
    selected = new TreeSet<ListEntry> ();
    if (sel != null)
      {
        for (ListEntry e : es)
          if (sel.contains (e))
            selected.add (e);
      }
  }

  /**
   * Get the set of selected entries.
   * @return Selected entry set.
   */
  public Set<ListEntry> getSelected ()
  {
    return selected;
  }

  /**
   * Get the number of groups.
   * @return Number of groups (shops).
   */
  @Override
  public int getGroupCount ()
  {
    if (shops == null)
      return 0;
    return shops.length;
  }

  /**
   * Return the group (shop) at the given position.
   * @param ind The index queried for.
   * @return The shop at this position.
   */
  @Override
  public Shop getGroup (int ind)
  {
    return shops[ind];
  }

  /**
   * Return a group (shop) ID.
   * @param ind The index queried for.
   * @return The entry's ID.
   */
  @Override
  public long getGroupId (int ind)
  {
    return shops[ind].getID ();
  }

  /**
   * Construct a view for a group.
   * @param ind The entry's index.
   * @param expanded Whether the group is expanded or not.
   * @param v A view that can be recycled.
   * @param parent The parent view group.
   */
  @Override
  public View getGroupView (int ind, boolean expanded, View v, ViewGroup parent)
  {
    if (v == null)
      {
        final LayoutInflater inf = LayoutInflater.from (context);
        v = inf.inflate (R.layout.list_group_row, null);
      }

    final TextView tv = (TextView) v.findViewById (R.id.listGroupName);
    tv.setText (shops[ind].getName ());

    return v;
  }

  /**
   * Get the number of childs (entries) of a group (shop).
   * @param gind Group index.
   * @return Number of entries in the selected group.
   */
  @Override
  public int getChildrenCount (int gind)
  {
    if (shops == null || entries == null)
      return 0;

    return entries.get (shops[gind]).size ();
  }

  /**
   * Return the entry at the given position in the selected group.
   * @param gind The group index.
   * @param ind The child index within the group.
   * @return The list entry at this position.
   */
  @Override
  public ListEntry getChild (int gind, int ind)
  {
    return entries.get (shops[gind]).get (ind);
  }

  /**
   * Return an entry's ID.
   * @param gind The group index.
   * @param ind The child index within the group.
   * @return The entry's ID.
   */
  @Override
  public long getChildId (int gind, int ind)
  {
    return getChild (gind, ind).getID ();
  }

  /**
   * Construct a view for a child.
   * @param gind The group index.
   * @param ind The entry's index within the group.
   * @param last Whether this is the last child within the group.
   * @param v A view that can be recycled.
   * @param parent The parent view group.
   */
  @Override
  public View getChildView (final int gind, final int ind, boolean last,
                            View v, ViewGroup parent)
  {
    if (v == null)
      {
        final LayoutInflater inf = LayoutInflater.from (context);
        v = inf.inflate (R.layout.list_entry_row, null);
        v.setTag (null);
      }

    /* We store the last row associated to a recycled view in the tag.
       If there is one, extract it and make sure to destroy
       the link between row and view.  */
    Row r;
    if (v.getTag () != null)
      {
        r = (Row) v.getTag ();
        r.unsetView ();
      }

    r = new Row (getChild (gind, ind), v);
    v.setTag (r);

    return v;
  }

  /**
   * Return whether we have stable IDs.
   * @return Always true.
   */
  @Override
  public boolean hasStableIds ()
  {
    return true;
  }

  /**
   * Check whether a child is selectable.
   * @param gind Group index.
   * @param ind Child index.
   * @return Always false.
   */
  @Override
  public boolean isChildSelectable (int gind, int ind)
  {
    return false;
  }

  /* ************************************************************************ */
  /* Row.  */

  /**
   * Class for one of the rows.  It manages checkbox clicking.
   */
  private class Row implements View.OnClickListener, View.OnLongClickListener
  {

    /** The entry it corresponds to.  */
    private final ListEntry entry;

    /** The full view (which has the onclick listener).  */
    private View view;

    /** The checkbox view.  */
    private CheckBox check;

    /**
     * Construct it for the given view.
     * @param e The entry for this row.
     * @param v The full row view.
     */
    public Row (ListEntry e, View v)
    {
      entry = e;
      view = v;

      check = (CheckBox) view.findViewById (R.id.listEntryCheck);
      check.setChecked (selected.contains (entry));

      view.setOnClickListener (this);
      view.setOnLongClickListener (this);

      TextView tv;
      tv = (TextView) view.findViewById (R.id.listEntryName);
      tv.setText (entry.getItem ().getName ());

      tv = (TextView) view.findViewById (R.id.listEntryTag);
      final String tag = entry.getTag ();
      if (tag == null)
        tv.setText ("");
      else
        tv.setText (tag);

      tv = (TextView) view.findViewById (R.id.listEntryPrice);
      final Double price = entry.getPrice ();
      if (price == null)
        tv.setText ("?");
      else
        tv.setText (priceFmt.format (price));
    }

    /**
     * Destroy link to view.  This is done when the view
     * is recycled.
     */
    public void unsetView ()
    {
      if (view != null)
        view.setOnClickListener (null);

      view = null;
      check = null;
    }

    /**
     * Handle click / toggle of the checkbox.
     * @param v Clicked view.
     */
    @Override
    public void onClick (View v)
    {
      assert (v == view);
      check.toggle ();

      if (check.isChecked ())
        selected.add (entry);
      else
        selected.remove (entry);
    }

    /**
     * Handle long click.
     * @param v Clicked view.
     */
    @Override
    public boolean onLongClick (View v)
    {
      assert (v == view);
      context.showItemDetails (entry.getItem ());
      return true;
    }

  }

}
