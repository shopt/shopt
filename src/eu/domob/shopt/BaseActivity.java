/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;

/**
 * Base class for activities that access the database.
 */
public class BaseActivity extends Activity
{

  /** Data access object used.  */
  protected DataAccess db;

  /**
   * Create the activity.
   * @param savedInstanceState The saved state.
   */
  @Override
  public void onCreate (Bundle savedInstanceState)
  {
    super.onCreate (savedInstanceState);

    db = new DataAccess (this);
    /* Database will be opened in onResume later on.  */
  }

  /**
   * Resume operation after being paused.
   */
  @Override
  protected void onResume ()
  {
    super.onResume ();
    db.open ();
  }

  /**
   * Pause operation.
   */
  @Override
  protected void onPause ()
  {
    db.close ();
    super.onPause ();
  }

  /**
   * Utility routine to show item details.
   * @param itm The item to show details for.
   */
  protected void showItemDetails (Item itm)
  {
    final Intent intent = new Intent (this, ItemDetails.class);   
    intent.putExtra (Shopt.MESSAGE_ITEM, itm);
    startActivity (intent);
  }

}
