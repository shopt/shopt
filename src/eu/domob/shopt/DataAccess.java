/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.SQLException;

import android.database.sqlite.SQLiteDatabase;

import android.util.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.TreeMap;

/**
 * The database access class.
 */
public class DataAccess
{

  /** Database helper class used to open the DB.  */
  private DatabaseHelper helper;

  /** Opened database.  */
  SQLiteDatabase db;

  /**
   * Construct it in the given context.
   * @param c Context.
   */
  public DataAccess (Context c)
  {
    helper = new DatabaseHelper (c);
    db = null;
  }

  /**
   * Open the database internally.
   */
  public void open () throws SQLException
  {
    assert (db == null);
    db = helper.getWritableDatabase ();

    Log.i (Shopt.TAG, "Database connection opened.");
  }

  /**
   * Close the database.
   */
  public void close ()
  {
    Log.i (Shopt.TAG, "Closing database.");

    helper.close ();
    db = null;
  }

  /**
   * Query for all shops in the database.
   * @return Array of shop data objects.
   */
  public Shop[] getShops ()
  {
    final String[] columns = new String[] { "_id", "name" };
    Cursor c = db.query ("shops", columns, null, null, null, null,
                         "`name` ASC");

    Shop[] res = new Shop[c.getCount ()];
    int i = 0;
    for (c.moveToFirst (); !c.isAfterLast (); c.moveToNext ())
      res[i++] = new Shop (c.getLong (0), c.getString (1));
    c.close ();
    assert (i == res.length);

    Log.d (Shopt.TAG,
           String.format ("Queried for shops, got %d entries.", res.length));
    return res;
  }

  /**
   * Delete the given shop.
   * @param s The shop to delete (its ID will be used).
   */
  public void deleteShop (Shop s)
  {
    final String[] idValue = new String[] { Long.toString (s.getID ()) };
    int cnt = db.delete ("shops", "`_id` = ?", idValue);
    if (cnt == 1)
      Log.d (Shopt.TAG,
             String.format ("Deleted shop '%s' with ID %d.",
                            s.getName (), s.getID ()));
    else
      Log.w (Shopt.TAG,
             String.format ("Delete shop '%s' with ID %d, deleted %d rows",
                            s.getName (), s.getID (), cnt));

    cnt = db.delete ("prices", "`shop` = ?", idValue);
    Log.d (Shopt.TAG,
           String.format ("Deleted %d orphaned price entries.", cnt));

    cnt = db.delete ("list", "`shop` = ?", idValue);
    Log.d (Shopt.TAG,
           String.format ("Deleted %d orphaned list entries.", cnt));
  }

  /**
   * Inserts a shop into the database.
   * @param s The shop to insert (should have ID -1).
   */
  public void insertShop (Shop s)
  {
    ContentValues vals = new ContentValues (1);
    vals.put ("name", s.getName ());
    final long id = db.insert ("shops", null, vals);
    if (id == -1)
      Log.e (Shopt.TAG,
             String.format ("Inserting shop '%s' failed.", s.getName ()));
  }

  /**
   * Query for all items in the database.
   * @return Array of item data objects.
   */
  public Item[] getItems ()
  {
    final String[] columns = new String[] { "_id", "name" };
    Cursor c = db.query ("items", columns, null, null, null, null,
                         "`name` ASC");

    Item[] res = new Item[c.getCount ()];
    int i = 0;
    for (c.moveToFirst (); !c.isAfterLast (); c.moveToNext ())
      res[i++] = new Item (c.getLong (0), c.getString (1));
    c.close ();
    assert (i == res.length);

    Log.d (Shopt.TAG,
           String.format ("Queried for items, got %d entries.", res.length));
    return res;
  }

  /**
   * Query for an item by its name.  This is used with input from the
   * "add list entry" text box.  It tries to look up an item and returns
   * the item only if exactly one match is found.  If none or multiple matches
   * are there, null is returned.  (Should ideally not happen if the user
   * uses the auto-completion.)
   * @param name The item's name to query for.
   * @return The item or null.
   */
  public Item getItemByName (String name)
  {
    final String[] columns = new String[] { "_id", "name" };
    Cursor c = db.query ("items", columns,
                         "`name` = ?", new String[] { name },
                         null, null, null);

    final int cnt = c.getCount ();
    if (cnt != 1)
      {
        Log.w (Shopt.TAG,
               String.format ("Found %d items matching name '%s'.",
                              cnt, name));
        c.close ();
        return null;
      }

    c.moveToFirst ();
    final Item res = new Item (c.getLong (0), c.getString (1));
    c.close ();

    return res;
  }

  /**
   * Delete the given item.
   * @param itm The item to delete (its ID will be used).
   */
  public void deleteItem (Item itm)
  {
    final String[] idValue = new String[] { Long.toString (itm.getID ()) };
    int cnt = db.delete ("items", "`_id` = ?", idValue);
    if (cnt == 1)
      Log.d (Shopt.TAG,
             String.format ("Deleted item '%s' with ID %d.",
                            itm.getName (), itm.getID ()));
    else
      Log.w (Shopt.TAG,
             String.format ("Delete item '%s' with ID %d, deleted %d rows",
                            itm.getName (), itm.getID (), cnt));

    cnt = db.delete ("prices", "`item` = ?", idValue);
    Log.d (Shopt.TAG,
           String.format ("Deleted %d orphaned price entries.", cnt));

    cnt = db.delete ("list", "`item` = ?", idValue);
    Log.d (Shopt.TAG,
           String.format ("Deleted %d orphaned list entries.", cnt));
  }

  /**
   * Inserts an item into the database.
   * @param itm The item to insert (should have ID -1).
   * @return The new item as Item object with ID set.
   */
  public Item insertItem (Item itm)
  {
    ContentValues vals = new ContentValues (1);
    vals.put ("name", itm.getName ());
    final long id = db.insert ("items", null, vals);
    if (id == -1)
      {
        Log.e (Shopt.TAG,
               String.format ("Inserting item '%s' failed.", itm.getName ()));
        return null;
      }

    return new Item (id, itm.getName ());
  }

  /**
   * Query for item prices.  This returns a Map indexed by Shop objects,
   * mapping shops to the prices the item has there.  Each entry in the
   * map means that the shop does have the item, but the price
   * may be null to indicate that it is not known.
   * @param itm The item to query for.
   * @return The shop-to-price mapping for the item.
   */
  public Map<Shop, Double> getItemShops (Item itm)
  {
    final String[] columns = new String[] { "shop", "price" };
    final String[] idValue = new String[] { Long.toString (itm.getID ()) };
    Cursor c = db.query ("prices", columns, "`item` = ?", idValue, null, null,
                         "`shop` ASC");

    final Map<Shop, Double> res = new TreeMap<Shop, Double> ();
    for (c.moveToFirst (); !c.isAfterLast (); c.moveToNext ())
      {
        final Shop key = new Shop (c.getLong (0));
        if (res.containsKey (key))
          Log.w (Shopt.TAG,
                 String.format ("Multiple entries for shop %d and item %s!",
                                key.getID (), itm.getName ()));

        if (c.isNull (1))
          res.put (key, null);
        else
          res.put (key, c.getDouble (1));
      }
    c.close ();

    Log.d (Shopt.TAG,
           String.format ("Queried for shops having item %s, got %d entries.",
                          itm.getName (), res.size ()));
    return res;
  }

  /**
   * Update the item price mappings.  This routine clears all entries
   * in the 'prices' table corresponding to the given item, and inserts
   * all represented by the map.
   * @param itm The item we are processing.
   * @param pm The price map.
   */
  public void setItemShops (Item itm, Map<Shop, Double> pm)
  {
    final String idString = Long.toString (itm.getID ());
    int cnt = db.delete ("prices", "`item` = ?", new String[] { idString });

    Log.d (Shopt.TAG,
           String.format ("Deleted %d old price mappings for '%s', adding %d.",
                          cnt, itm.getName (), pm.size ()));

    final ContentValues vals = new ContentValues (3);
    vals.put ("item", itm.getID ());
    for (Map.Entry<Shop, Double> e : pm.entrySet ())
      {
        final Shop s = e.getKey ();
        final Double p = e.getValue ();

        vals.put ("shop", s.getID ());
        if (p == null)
          vals.putNull ("price");
        else
          vals.put ("price", p);

        final long id = db.insert ("prices", null, vals);
        if (id == -1)
          Log.e (Shopt.TAG,
                 String.format ("Inserting price entry failed.",
                                itm.getName ()));
      }

    final String query = "SELECT `shop` FROM `prices`"
                         + "  WHERE `item` = ?";
    cnt = db.delete ("list", "`item` = ? AND `shop` NOT IN (" + query + ")",
                     new String[] { idString, idString });
    Log.d (Shopt.TAG,
           String.format ("Deleted %d now-invalid entries from the list.",
                          cnt));
  }

  /**
   * Query for all list entries in the database.
   * @return Array of list entry data objects.
   */
  public ListEntry[] getListEntries ()
  {
    final String[] columns = new String[] { "_id", "shop", "item", "tag",
                                            "shop_name", "item_name", "price" };
    Cursor c = db.query ("extended_list", columns, null, null, null, null,
                         "`_id` ASC");

    ListEntry[] res = new ListEntry[c.getCount ()];
    int i = 0;
    for (c.moveToFirst (); !c.isAfterLast (); c.moveToNext ())
      {
        final Shop s = new Shop (c.getLong (1), c.getString (4));
        final Item itm = new Item (c.getLong (2), c.getString (5));

        String tag = null;
        if (!c.isNull (3))
          tag = c.getString (3);

        Double price = null;
        if (!c.isNull (6))
          price = c.getDouble (6);
        res[i++] = new ListEntry (c.getLong (0), itm, s, tag, price);
      }
    c.close ();
    assert (i == res.length);

    Log.d (Shopt.TAG,
           String.format ("Queried for list, got %d entries.", res.length));
    return res;
  }

  /**
   * Delete the given list entry.
   * @param e The entry to delete (its ID will be used).
   */
  public void deleteListEntry (ListEntry e)
  {
    final String[] idValue = new String[] { Long.toString (e.getID ()) };
    int cnt = db.delete ("list", "`_id` = ?", idValue);
    if (cnt == 1)
      Log.d (Shopt.TAG,
             String.format ("Deleted list entry ID %d.", e.getID ()));
    else
      Log.w (Shopt.TAG,
             String.format ("Delete list entry ID %d, deleted %d rows",
                            e.getID (), cnt));
  }

  /**
   * Inserts a list entry into the database.
   * @param e The entry to insert (should have ID -1).
   */
  public void insertListEntry (ListEntry e)
  {
    ContentValues vals = new ContentValues (3);
    vals.put ("shop", e.getShop ().getID ());
    vals.put ("item", e.getItem ().getID ());
    final String tag = e.getTag ();
    if (tag == null)
      vals.putNull ("tag");
    else
      vals.put ("tag", tag);

    final long id = db.insert ("list", null, vals);
    if (id == -1)
      Log.e (Shopt.TAG, "Inserting new list entry failed.");
  }

}
