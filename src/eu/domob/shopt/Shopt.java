/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

/**
 * Utility class collecting some basic stuff.
 */
public class Shopt
{

  /** Log tag to use.  */
  public static final String TAG = "Shopt";

  /** Intent message key for passing the item.  */
  public static final String MESSAGE_ITEM = "eu.domob.shopt.ITEM";

}
