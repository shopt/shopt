/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.content.Intent;

import android.os.Bundle;

import android.util.Log;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Set;

/**
 * Activity to for adding an entry to the list.
 */
public class AddEntry extends BaseActivity
{

  /** The item for which the details are shown.  */
  private Item itm;

  /** The tag entry field.  */
  private EditText tagEntry;

  /** Data adapter for the shown list.  */
  private AddEntryAdapter data;

  /** Selected set, to store it between pause/resume calls.  */
  private Set<Shop> selected;

  /**
   * Create the activity.
   * @param savedInstanceState The saved state.
   */
  @Override
  public void onCreate (Bundle savedInstanceState)
  {
    super.onCreate (savedInstanceState);
    setContentView (R.layout.add_entry);

    tagEntry = (EditText) findViewById (R.id.editTag);

    /* Interpret the intent.  */
    final Intent intent = getIntent ();
    itm = (Item) intent.getSerializableExtra (Shopt.MESSAGE_ITEM);

    /* Show item name as "heading".  */
    final TextView tv = (TextView) findViewById (R.id.itemName);
    tv.setText (itm.getName ());

    /* Set up the list of shops/prices.  */
    final ListView list = (ListView) findViewById (R.id.addEntryShops);
    data = new AddEntryAdapter (this);
    list.setAdapter (data);
    selected = null;
  }

  /**
   * Resume operation after being paused.
   */
  @Override
  protected void onResume ()
  {
    super.onResume ();

    data.setData (db.getShops (), db.getItemShops (itm), selected);
    data.notifyDataSetChanged ();
  }

  /**
   * Pause and store data before that.
   */
  @Override
  protected void onPause ()
  {
    selected = data.getSelected ();
    super.onPause ();
  }

  /**
   * Handle click on the "ok" button.
   * @param v The button view.
   */
  public void onOkClick (View v)
  {
    selected = data.getSelected ();
    Log.d (Shopt.TAG,
           String.format ("Selected %d shops for listing item '%s'.",
                          selected.size (), itm.getName ()));

    for (Shop s : selected)
      {
        String tag = tagEntry.getText ().toString ();
        if (tag.isEmpty ())
          tag = null;
        db.insertListEntry (new ListEntry (itm, s, tag));
      }

    finish ();
  }

  /**
   * Handle click on the "cancel" button.
   * @param v The button view.
   */
  public void onCancelClick (View v)
  {
    /* Do nothing, just finish the activity without adding the entry.  */
    finish ();
  }

  /**
   * Create the options menu.
   * @param menu Inflate menu there.
   * @return True since we always succeed.
   */
  @Override
  public boolean onCreateOptionsMenu (Menu menu)
  {
    final MenuInflater inflater = getMenuInflater ();
    inflater.inflate (R.menu.add_entry, menu);
    return true;
  }

  /**
   * Perform action on click in menu.
   * @param mitm Selected item.
   * @return True iff event was processed.
   */
  @Override
  public boolean onOptionsItemSelected (MenuItem mitm)
  {
    switch (mitm.getItemId ())
      {
      case R.id.itemDetails:
        showItemDetails (itm);
        return true;

      default:
        return super.onOptionsItemSelected (mitm);
      }
  }

}
