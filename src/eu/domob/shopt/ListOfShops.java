/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.os.Bundle;

import android.util.SparseBooleanArray;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

/**
 * Activity for editing the list of shops.
 */
public class ListOfShops extends BaseActivity
{

  /** The list view.  */
  private ListView view;

  /** Array adapter with the shop list data.  */
  private ArrayAdapter<Shop> data;

  /** Input field for the new shop's name.  */
  private EditText shopNameInput;

  /**
   * Create the activity.
   * @param savedInstanceState The saved state.
   */
  @Override
  public void onCreate (Bundle savedInstanceState)
  {
    super.onCreate (savedInstanceState);
    setContentView (R.layout.shops);

    /* Set up the list view with empty data (for now).  */
    view = (ListView) findViewById (R.id.shopList);
    data = new ArrayAdapter<Shop> (this,
                  android.R.layout.simple_list_item_multiple_choice);
    view.setAdapter (data);

    shopNameInput = (EditText) findViewById (R.id.shopName);
  }

  /**
   * Resume operation after being paused.
   */
  @Override
  protected void onResume ()
  {
    super.onResume ();
    updateList ();
  }

  /**
   * Update the data shown in the list from the DB.
   */
  private void updateList ()
  {
    final Shop[] shops = db.getShops ();
    data.clear ();
    for (Shop s : shops)
      data.add (s);
    data.notifyDataSetChanged ();
  }

  /**
   * Handle "add" button click.
   * @param v The button view.
   */
  public void onAddClick (View v)
  {
    final String name = shopNameInput.getText ().toString ();
    if (!name.isEmpty ())
      {
        final Shop s = new Shop (name);
        db.insertShop (s);
        updateList ();
        shopNameInput.setText ("");
      }
  }

  /**
   * Create the options menu.
   * @param menu Inflate menu there.
   * @return True since we always succeed.
   */
  @Override
  public boolean onCreateOptionsMenu (Menu menu)
  {
    final MenuInflater inflater = getMenuInflater ();
    inflater.inflate (R.menu.shops, menu);
    return true;
  }

  /**
   * Perform action on click in menu.
   * @param itm Selected item.
   * @return True iff event was processed.
   */
  @Override
  public boolean onOptionsItemSelected (MenuItem itm)
  {
    switch (itm.getItemId ())
      {
      case R.id.shopListDelete:
        final SparseBooleanArray selected = view.getCheckedItemPositions ();
        for (int i = 0; i < data.getCount (); ++i)
          if (selected.get (i))
            db.deleteShop (data.getItem (i));
        view.clearChoices ();
        updateList ();
        return true;

      default:
        return super.onOptionsItemSelected (itm);
      }
  }

}
