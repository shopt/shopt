/*
    Shopt.  Shopping list optimiser.
    Copyright (C) 2014  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package eu.domob.shopt;

import android.content.Context;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.Map;

/**
 * Custom adapter class for the item details activity.  It yields
 * a list of shops and for each shop whether the item is present
 * there or not and its price.
 */
public class ItemDetailsAdapter extends BaseAdapter
{

  /** Context for created views.  */
  private final Context context;

  /** List of shops.  */
  private Shop[] shops;

  /** Mapping of shops to prices.  */
  private Map<Shop, Double> prices;

  /**
   * Construct the adapter without data for now.
   * @param c The context to use.
   */
  public ItemDetailsAdapter (Context c)
  {
    context = c;
    shops = null;
  }

  /**
   * Set the data.
   * @param s List of all shops in the DB.
   * @param p Price map.
   */
  public void setData (Shop[] s, Map<Shop, Double> p)
  {
    shops = s;
    prices = p;
  }

  /**
   * Get the data as shop-to-price map.
   * @return Shop-to-price map.
   */
  public Map<Shop, Double> getData ()
  {
    return prices;
  }

  /**
   * Return item count, which is the number of shops.
   * @return Number of shops in the dataset.
   */
  @Override
  public int getCount ()
  {
    if (shops == null)
      return 0;
    return shops.length;
  }

  /**
   * Return the entry (shop) at the given position.
   * @param ind The index queried for.
   * @return The shop at this position.
   */
  @Override
  public Shop getItem (int ind)
  {
    return shops[ind];
  }

  /**
   * Return an entry (shop) ID.
   * @param ind The index queried for.
   * @return The entry's ID.
   */
  @Override
  public long getItemId (int ind)
  {
    return shops[ind].getID ();
  }

  /**
   * Construct a view for an entry.
   * @param ind The entry's index.
   * @param v A view that can be recycled.
   * @param parent The parent view group.
   */
  @Override
  public View getView (int ind, View v, ViewGroup parent)
  {
    if (v == null)
      {
        final LayoutInflater inf = LayoutInflater.from (context);
        v = inf.inflate (R.layout.item_details_row, null);
        v.setTag (null);
      }

    /* We store the last row associated to a recycled view in the tag.
       If there is one, extract it and make sure to destroy
       the link between row and view.  */
    Row r;
    if (v.getTag () != null)
      {
        r = (Row) v.getTag ();
        r.unsetView ();
      }

    r = new Row (shops[ind], v);
    v.setTag (r);

    return v;
  }

  /* ************************************************************************ */
  /* Row.  */

  /**
   * Class for one of the rows (corresponding to a shop).  It holds the checkbox
   * and the price entry widgets and manages basic UI for the row.
   */
  private class Row implements View.OnClickListener, TextWatcher
  {

    /** The shop it corresponds to.  */
    private final Shop shop;

    /** The checkbox view.  */
    private CheckBox check;

    /** The price entry view.  */
    private EditText entry;

    /**
     * Construct it for the given view.
     * @param s The shop for this row.
     * @param v The full row view.
     */
    public Row (Shop s, View v)
    {
      shop = s;

      check = (CheckBox) v.findViewById (R.id.shopHasItem);
      entry = (EditText) v.findViewById (R.id.itemPrice);

      check.setText (shop.getName ());
      check.setOnClickListener (this);
      entry.addTextChangedListener (this);

      /* Set status from the prices map.  */
      if (prices.containsKey (shop))
        {
          check.setChecked (true);
          final Double p = prices.get (shop);
          if (p != null)
            entry.setText (p.toString ());
          else
            entry.setText ("");
        }
      else
        {
          check.setChecked (false);
          entry.setText ("");
        }

      updateUI ();
    }

    /**
     * Destroy link to view.  This is done when the view
     * is recycled.
     */
    public void unsetView ()
    {
      if (check != null)
        check.setOnClickListener (null);
      if (entry != null)
       entry.removeTextChangedListener (this);

      check = null;
      entry = null;
    }

    /**
     * Handle click / toggle of the checkbox.
     * @param v Clicked view.
     */
    @Override
    public void onClick (View v)
    {
      assert (v == check);
      updatePriceMap ();
      updateUI ();
    }

    /**
     * Handle text changes to the entry.  This is used to update
     * the internal data store accordingly.
     * @param s Editable string.
     */
    @Override
    public void afterTextChanged (Editable s)
    {
      updatePriceMap ();
    }

    /* TextWatcher routines that we do not need.  */
    @Override
    public void beforeTextChanged (CharSequence s, int start, int count,
                                   int after)
    {}
    @Override
    public void onTextChanged (CharSequence s, int start, int before, int count)
    {}

    /**
     * Update after status change.  This enables/disables the text entry
     * according to the checkbox status.
     */
    private void updateUI ()
    {
      entry.setEnabled (check.isChecked ());
    }

    /**
     * Internally update the price map in case something changed.
     */
    private void updatePriceMap ()
    {
      if (!check.isChecked ())
        prices.remove (shop);
      else
        {
          final String priceStr = entry.getText ().toString ();
          if (priceStr.isEmpty ())
            prices.put (shop, null);
          else
            prices.put (shop, Double.parseDouble (priceStr));
        }
    }

  }

}
